import Vue from "vue";
import App from "./App.vue";
import router from "../router";
import Element from "element-ui";
import "element-ui/lib/theme-chalk/index.css";

import { registerMicroApps, start } from "qiankun";
// 定义要整合的微应用列表
const apps = [
  {
    name: "dist-vue", // 应用的名字
    entry: "/dist-vue/", // 默认加载这个html，解析里面的js动态的执行（子应用必须支持跨域，内部使用的是 fetch）
    container: "#childTwo", // 要渲染到的容器名id
    activeRule: "#/dist-vue", // 通过哪一个路由来激活
  },
];
console.log("进入主应用");
// 注册应用
registerMicroApps(apps);
// 开启应用
start();

Vue.config.productionTip = false;

Vue.use(Element);
new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
