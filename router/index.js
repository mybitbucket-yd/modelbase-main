import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);

const routes = [
  {
    path: "/", //重点：路由匹配规则一定要加/*，因为要匹配子应用路由
    name: "home",
    redirect: "/dist-vue",
  },
];

const router = new VueRouter({
  mode: "hash",
  //   base: process.env.BASE_URL,
  routes,
});

export default router;
